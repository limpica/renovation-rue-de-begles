Rénovation d'un appartement Rue de Bègles, Bordeaux
===================================================
![jardin](./img/jardin-automne21.jpg)
![projet3D](./img/projet-3D-perspective.png)

Ce site présente la documentation des travaux de rénovation d'un appartement situé à Bordeaux dans la rue de Bègles. L'appartement de 48m2 environ (loi Carez) est situé en rez-de-jardin. C'est un T3  composé d'une cuisine-séjour, une salle d'eau, un WC et un cagibi. 2 chambres avec mezzanine sont elles desservies par un dégagement accessible directement depuis le séjour. 

![pano séjour](./img/ante-pano-sejour.jpg)


Dans cette rénovation nous avons au cherché au maximum à :
* **retrouver le cachet perdu** soit en révélant des matériaux ancien, soit en remplaçant certains matériaux actuels
* **optimiser la hauteur sous plafond disponible** de 3m55 pour créer notamment dans le séjour des espaces de vie ou de rangement supplémentaires

**L'objectif globale de cette rénovation est de permettre à une famille avec 2 enfants de pouvoir vivre en harmonie en disposant d'un espace commun fonctionnel et fluide, mais aussi d'espaces privatifs, même de petite taille, permettant de retrouver une intimité.**


La rénovation de cet appartement a donc consisté à :
- Révéler et restaurer des parties de murs en pierre
- Révéler une partie des poutres et solivages en bois
- Agrandire le séjour avec la création d'une mezzanine dans le séjour au dessus de l'ancien emplacement de la cuisine et de la salle de bain 
- Augmenter la sensation d'espace et de lumière dans le séjour en déposant le cagibi et mieux  distribuer les fonctions de cette pièce principale en déplaçant la cuisine vers la vue du jardin. 
- Réfection complète et aggrandissement de la salle de bain avec des matériaux plus nobles et en créant une cloison porteuse pour la mezzanine
- Agrandissement de la surface utile de la plus grande chambre:  reconstruction en plus grand de la mezzanine qui s'appuie elle aussi sur une cloison porteuse séparant les 2 chambres; cette chambre sera partagée entre les 2 enfants qui pourront soit dormir ensemble sur la mezzanine aggrandie, soit se partager entre l'espace supérieur et inférieur.
- Optmisation de la hauteur de la mezzanine dans la 2ème chambre: reconstruction de la mezzanine dans les mêmes proportions mais avec un plancher plus fin et placé plus bas. Cette chambre sera utilisée par les parents qui dormiront sur la mezzanine, l'espace inférieur sera lui réagencé en rangements et espaces bureaux







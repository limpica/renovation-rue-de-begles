# Summary

* [Introduction](README.md)
* [État des lieux](état-des-lieux.md)
* [Phase 1 : démolition](demolition.md) 
* [Phase 2 : Restauration de la pierre et des poutres](restauration-pierre.md) 
* [Phase 3 : Charpente et mezzanines](charpente-mezza.md) 
* [Phase 3 : cloisonement et plafonds](cloisonement-plafonds.md) 
* [Phase 4 : Cuisine et SdB](cuisine-sdb.md)
* [Phase 5 : Agencements 1ère partie](agencements-part1.md)
* [Annexes](annexes.md)

  


Annexes
=======

# Entreprises ayant travaillé sur ce projet

## Plombier-électricien
## Charpentier
## Menuisier


# Fournisseurs de matériaux recommandés

Voici une sélection d'entreprise à taille modérée fabriquant ou fournissant des produits de haute qualité et que nous avons mis en oeuvre sur ce chantier

## Planchers Boucaud (Lyon)
## Scierie-Parquetterie Vallereuil (Vallereuil, Dordogne)
## Carrelage Winckelmans (Lommes, Nord)
## Affuteur Desrousseaux (Cenon)
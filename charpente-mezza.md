Construction des mezzanines
===========================


# Projet

Le projet est d'installer 3 mezzanines c'est à dire dans chacune des pièces de l'appartement. Chaque mezzanine sera suspendu entre d'une part une structure poteau-poutre dissimulée dans les cloisons, et d'autre part des murs porteurs ou semi-porteurs en pierre de taille (ces murs sont les parois les plus extérieures dans les plans ci-après). 

Le type de structure envisagé pour chaque mezzanine est le plancher auto-porteur, d'une épaisseur de 80mm et reposant de part et d'autre sur une cornière métallique fixée dans le mur ou directement sur les structures poteau-poutre dissimulée dans les cloisons.

L'emplacement, la hauteur d'installation, et les dimensions en longueur et largeur de chaque mezzanine sont reportées dans les plans ci-après.

# Conception - Plans


<div align="center">
<img src="img/charpente/perspective.png"/>
<i>Vue en perspective du projet</i></div>


<div align="center">
<img src="img/charpente/coupe-BB-vue-arriere-chambre2-sejour.png"/>
<i>Coupe B-B selon le plan XZ de la vue perspective devant la charpente entre les mezzanine des chambres 1 et 2</i></div>


<div align="center">
<img src="img/charpente/coupe-AA-vue-gauche-devant-sdb.png">
<i>Coupe AA selon le plan YZ de la vue perspective devant la mezzanine du séjour</i></div>


<div align="center">
<img src="img/charpente/vue-dessus.png">
<i>Vue de dessus des 3 mezzanines</i></div>

# Fabrication

## Mezzanine du séjour


<div align="center">
<img src="img/charpente/charpente-sejour-demarrage.JPG">
<br/>
<p><i>Démarrage par pose des lisses basses</i></p></div>


<div align="center">
<img src="img/charpente/solivage-séjour.JPG" />
<br/>
<i>Le plancher au dessus de la Sdb sera réalisé avec un solivage classique réalisé en partie avec de la récupération des anciennes mezzanines. Les muraillères sont vissées au mur via des tiges filetées (diam. 10) fixées avec du scellement chimique </i></div>

<div align="center">
<img src="img/charpente/charpente-sdb-terminée.JPG" /><br/>
<i>Charpente de la salle de bain portant la mezzanine du séjour. Juste avant la pose de l'isolant et le placage.</i><br/></div>

<br/>

<div align="center">
<img src="img/charpente/sejour-detail-cornière.JPG"><br/>
<i>Détail de la cornière métallique (70x70mm) vissées sur la charpente et servant d'appui pour les lattes de plancher autoporteur</i><br/></div><br/>

<div align="center">
<img src="img/charpente/cornières-séjour.JPG"><br/>
<i>De l'autre côté de la charpente la cornière est fixés par scellement dans le mur mais également en appui sur des poteaux eux aussi scellés dans le mur</i></div><br/>

<div align="center">
<img src="img/charpente/mezza-sejour-debutr.JPG"><br/>
<i>Debut du montage du plancher autoporteur</i></div><br/>

<div align="center">
<img src="img/charpente/mezza-sejour-finie.JPG"><br/>
<i>Fin du montage de la mezzanine du séjour. La dernière latte est délignée, poncée, chanfreinée puis vissée. Les vis sont dissimulées derrière des bouchons en bois usinés avec des couples de mèches permettant de réaliser des trous et bouchons au même diamètre</i></div><br/>


## Mezzanine des chambres

<div align="center">
<img src="img/charpente/charpentes-chambres-jour.JPG"><br/>
<i>Charpente qui servira d'appui aux mezzanines des 2 chambres</i></div><br/>

<div align="center">
<img src="img/charpente/mezza-ch-test-des-bretons.JPG"><br/>
<i>Début du montage du plancher dans la chambre 1 et test dit "des bretons"</i></div><br/>

<div align="center">
<img src="img/charpente/charpente-dégagement.JPG"><br/>
<i>Fin du montage du plancher dans la chambre 1 et réalisation d'un morceau de cloison porteuse qui formera un dégagement pour l'accès séparé aux chambres 1 et 2</i></div><br/>

<div align="center">
<img src="img/charpente/charoente-dégagement2.JPG"><br/>
<i>Cloison porteuse du dégagement vu de la chambre 2</i></div><br/>

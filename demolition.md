Démolition
==========

# Dépose des plafonds et découverte des poutres et solives en bois
Les faux plafonds anciens sont déposés pour récupérer un maximum de hauteur sous plafond, en particulier sous les mezzanines

![au dessus mezza](img/demolition/demol-au-dessus-mezza.JPG)

Ces anciens faux plafonds étaient réalisés à l'aide de lattis de bois cloués sur des solives et ensuite enduits d'une couche de platre de 2 cm environ.

![au dessus fx plaf](img/demolition/au-dessus-fx-plaf.JPG)

![demol-1st-steps.JPG](img/demolition/demol-1st-steps.JPG)

La dépose a été réalisée avec une scie sabre électrique sur batterie. Travail bien physique !

![démolisseurs](img/demolition/demolition-wo-men.JPG)

![démolisseurs](img/demolition/IMG_1238.JPG)

Une fois terminée, cette phase de démolition a révélé les solives et grosses poutres en pin maritime. Ces poutres révèlent quant à elle une  teinte orange-rosée lorsqu'on retire la couche de peinture verte qui avait été appliquée.

![demol séjour](img/demolition/demol-plaf-sejour.jpg)

![demol ch1](img/demolition/demol-plaf-sejour.jpg)


# dépose de la salle de bain
La salle de bain sera finalement entièrement déposée. Nous avions envisagé un temps de carreler directement sur l'ancien carrelage, mais cette approche ne garantissait pas de bons résultats pour la pose de la mosaïque que nous souhaitions.

Cette dépose se révéla de plus salutaire et utile pour reprendre de la hauteur sous plafond dans la salle de bain. En effet un plancher en béton y avait été coulé pour permettre la réalisation de la douche à l'italienne en augmentant la pente de l'écoulement. Cette dalle était par contre inutilement haute, à 15cm, car l'écoulement présentait un double coudage à 90° qui aurait pu être évité.


![dalle sdb](img/demolition/demol-sdb.JPG)

# La difficile dépose de l'imposte et du cagibi

La réfection de la cuisine et de la salle de bain se sont tout de suite imposées à nous à la fois à cause des matérieux utilisés, mais aussi de la disposition. La création de la mezzanine nous est apparue comme une première étape dans la conception, puis une fois la démolition entamée, nous avons appris à mieux cerner l'espce et les circulations possible. 

![imposte déposée](img/imposte-déposée.JPG)

C'est à ce moment que la question de la conservation du cagibi et de l'imposte s'est posée. Dans la première version du projet ils étaient tout deux conservés tels quels. Puis le besoin de faciliter l'entrée de la lumière nous a poussé à déposé l'imposte. 

Une fois l'imposte déposée, le cagibi ancien nous est apparu comme déséquilibré par rapport à la nouvelle distribution de l'espace que nous souhaitions. Nous voulions en effet faciliter la circulation de lumière et la vue vers le jardin, et la dépose du cagibi s'est alors imposée avec d'abord l'idée de reconstruire un placard à l'empreinte au sol plus petite mais situé au même endroit.

![cagibi déposé](img/demolition/cagibi-déposé+mur-graté.jpg)






Restauration des murs intérieurs en pierre et des poutres et solivages en bois
==============================

# Pierre 
## Révélation de la pierre

Cette opération consiste simplement en un "décroûtage" des enduits de ciment et de plâtre qui recouvrent la pierre

![pierre1](img/pierre/revel-pierre-sejour.jpg)
![pierre3](img/pierre/revel-pierre-ch1_2.jpg)
![pierre2](img/pierre/revel-pierre-ch1.jpg)

Une fois cette phase achevée, les anciens joints sont ouverts et les pierres fragiles ou trop dégradées déposées.

## Reconstruction des assemblages

La mise à nue des murs en pierre a révélé un assemblage de pierres d'assez basse qualité, hormis pour les conduits de cheminées, qui étaient sans doute toujours laissé nus en raison du rayonnement de chaleur. Le tailleur  de pierre nous a donc proposé de reconstruire des parties de murs en utilisant des pierres de l'immeuble récupérées du démontage de cheminées. Ces pierres ont été découpées et taillées dans l'épaisseur. Le mur en pierre a ensuite été creusé aux futurs emplacements de ces nouvelles pierres sur 5 à 10 cm en fonction de la qualité du fond.

![pierre-reconstruction](img/pierre/reconstruction-assemblage-pierre-ch1.jpg)

![pierre-reconstruction2](img/pierre/reconstruction-séjour.jpg)

## Finition
### Séjour
![pierre-finition1](img/pierre/finition-séjour_1.JPG)
![pierre-finition2](img/pierre/finition-séjour_2.jpg)
![pierre-finition6](img/pierre/resultat-séjour.jpg)
![pierre-finition7](img/pierre/resultat-séjour1.jpg)

### Chambres

![pierre-finition3](img/pierre/resultat-ch1.jpg)
![pierre-finition4](img/pierre/resultat-ch2.jpg)
![pierre-finition5](img/pierre/resultat-détail.jpg)

# Restauration des poutres et solivages en bois

## Décapage de la poutre et des solives du séjour

![bois](img/IMG_20211104_133339.jpg)
![bois](img/IMG_20211222_192743.jpg)

## Décapge de la poutre traversant les 2 chambres
![bois](img/IMG_20211024_110953.jpg)





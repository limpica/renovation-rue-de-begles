État des lieux de l'existant
============================

3 pièces principales:
* Séjour
![pano séjour](./img/ante-pano-sejour.jpg)
* Chambre parentale avec mezzanine et cheminée (chambre 1)
![ch1](img/ante_pano-ch1.jpg)
* Chambre d'enfant avec mezzanine (chambre 2)
![ch2](img/ante-pano-ch2.jpg)

Plan de l'état avant travaux

![plan2D](img/plan-interieur-2D-etat-achat.png)


# Premiers constats: il y a un potentiel de cachet, mais sous exploité

En entrant dans l'appartement, le séjour présente, dans sa partie gauche, une vue sur la partie supérieur d'un conduit de cheminée en pierre. La cheminée elle-même ayant été déposé, l'espace résiduelle est occupé par un canapé, et cette zone constitue une partie salon qui jouxte directement la partie cuisine. Ce conduit de cheminée constitue l'unique présence de la pierre visible à l'intérieur, bien que tous les murs extérieurs ainsi que celui séparant l'appartement du couloir de l'immeuble soient en pierre. Nous découvrirons cependanr plus tard que la qualité du montage de pierre sur les parties autres que les conduits de cheminées est plutôt basse et n'offre qu'un potentiel limité de révélation de la pierre.

![cheminee](./img/ante-cheminee-pierre.jpg)

Une fois entré dans l'appartement, on se retrouve entre la partie salon ouvrant vers la gauche sur la terrasse et le jardin, et la partie cuisine occupant la droite du séjour. Le salon n'est en fait qu'un passage entre la cuisine et l'accès au jardin qui se fait en traversant un ancien passage de porte dont l'imposte a été conservée. Cet ancienne porte était fixée dans une cloison qui sépare toujours le séjour du cagibi (porte à gauche, non visible sur la prochaine photo) et des WC (à droite sur la photo). La porte en bois (rénovée) donnant accès au jardin est située quant à elle au milieu.

![séjour vers jardin](img/ante-vue-jardin-depuis-cuisine.jpg) 

Les précédants occupants ont donc bien eu un souhait de préserver une partie du cachet en conservant la cheminé, l'imposte en bois et les moulures de l'ancien passage vers le jardin, ainsi que les portes anciennes en bois (WC et cagibi). Le problème est que beaucoup d'autres matériaux utilisés dans le reste de l'agencement (sdb, cuisine) sont de bien moins bonne facture

# Les choses à reprendre : cuisine, salle d'eau, WC

La cuisine équipé occupe une bonne moitié du séjour sur la droite en entrant. L'optimisation recherchée consistait à exploiter une partie du plan de travail comme table à manger. Les matériaux employé pour la cuisine sont du standard actuel: aggloméré stratifié de qualité moyenne à basse. Dans un souci écologique, nous tâcherons de réutiliser les caissons.

![cuisine](img/ante-vue-cuisine-sdb.jpg)

Dans le coin droit de la pièce, une porte coulissante vitrée dissimule la salle de bain qui consiste en un espace lavabo et une douche à l'italienne. Cette intégration de la salle de bain dans le séjour n'est pas très heureuse car l'intimité n'est pas totale malgré l'opacité du verre, et cet espace n'est donc pas bien séparé du reste du séjour. Un carrelage de qualité standard basse, sans cohérence esthétique particulière est utilisé pour le sol et les murs. Le même carrelage est également utilisé dans les WCs.

![sdb](img/ante-sdb.jpg)

